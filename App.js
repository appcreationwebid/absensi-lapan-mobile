import React, { Component } from 'react'
import { Text, View } from 'react-native'
import Routes from './src/routes'
import {Provider} from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { Persistor, Store } from './src/services/redux/Store'

class App extends Component {
  render() {
    return (
      <Provider store={Store}>
        <PersistGate loading={null} persistor={Persistor}>
          <Routes />
        </PersistGate>
      </Provider>
    )
  }
}

export default App


