import { ToastAndroid } from "react-native";


const CToastAndroid = (message, type) => {
    let outType = type === 'short' ?  ToastAndroid.SHORT : ToastAndroid.LONG
    return ToastAndroid.show(message, outType);
}

export default CToastAndroid