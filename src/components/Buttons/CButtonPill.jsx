//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { PRIMARY } from '../../services/styles/color';

// create a component
const CButtonPill = ({onPress, loading}) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.buttonContainer(loading)} onPress={onPress}>
                <View style={styles.buttonSection}>
                    <Text style={styles.textSection}>LOGIN</Text>
                </View>
            </TouchableOpacity>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        
    },
    buttonContainer: (loading) => ({
        backgroundColor:!loading?PRIMARY:'grey',
        alignItems:'center',
        width: '100%',
        borderRadius:50,
    }),
    buttonSection:{
        margin:15
    },
    textSection:{
        color:'white',
        fontSize:18,
        fontWeight:'bold'
    }
});

//make this component available to the app
export default React.memo(CButtonPill);
