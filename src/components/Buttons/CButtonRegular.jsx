//import liraries
import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { PRIMARY } from '../../services/styles/color';

// create a component
const CButtonRegular = ({title, onPress, loading, width}) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.buttonContainer(loading,width)} onPress={onPress}>
                <View style={styles.buttonSection}>
                    <Text style={styles.textSection}>{title}</Text>
                </View>
            </TouchableOpacity>
        </View>
    );
};

CButtonRegular.defaultProps = {
    width:10
}

// define your styles
const styles = StyleSheet.create({
    container: {
        
    },
    buttonContainer: (loading,width) => ({
        backgroundColor:!loading?PRIMARY:'grey',
        alignItems:'center',
        width: width*20,
        borderRadius:10,
    }),
    buttonSection:{
        margin:15
    },
    textSection:{
        color:'white',
        fontSize:16,
        fontWeight:'bold'
    }
});

//make this component available to the app
export default React.memo(CButtonRegular);
