//import liraries
import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TextInput, Animated } from 'react-native';
import { PRIMARY } from '../../services/styles/color';

// create a component
const CInputFloatLabel = ({label, value, onChangeText, autoCapitalize, keyboardType, autoComplete, textContentType, password, props, style}) => {
    console.log('CInputFloatLabel')
    const [focus, setFocus] = useState(false)
    
    const handleFocus = () => setFocus(true)
    const handleBlur = () => setFocus(value?true:false)
    
    useEffect(() => {
        Animated.timing(animatedFocus,{
            useNativeDriver:false,
            toValue: focus || value ? 1 : 0,
            duration:200
        }).start()
    },[focus])
    
    let animatedFocus = new Animated.Value(value?1:0)
    return (
        <View style={[styles.container, {...style}]}>
            <Animated.Text style={styles.labelSection(focus, animatedFocus)}>{label}</Animated.Text>
            <TextInput {...props} style={styles.textInputSection(focus)} 
                onFocus={handleFocus} onBlur={handleBlur} 
                value={value} onChangeText={onChangeText} 
                secureTextEntry={password}
                autoCapitalize={autoCapitalize}
                keyboardType={keyboardType}
                textContentType={textContentType}
                autoCompleteType={autoComplete}
            />
        </View>
    );
};

const compare = (prevProps, nextProps) => {
    const equal = JSON.stringify(prevProps) === JSON.stringify(nextProps)
    return equal
}

//make this component available to the app
export default React.memo(CInputFloatLabel, compare);

// define your styles
const styles = StyleSheet.create({
    container: {
        marginTop:15
    },
    labelSection: (focus, animatedFocus) => ({
        position: 'absolute',
        left: 2,
        top:animatedFocus.interpolate({
            inputRange:[0,1],
            outputRange:[15,-15]
        }),
        fontSize:animatedFocus.interpolate({
            inputRange: [0,1],
            outputRange:[20,17]
        }),
        color:animatedFocus.interpolate({
            inputRange: [0,1],
            outputRange:['gray', PRIMARY]
        }),
        fontWeight: focus?'bold':'normal'
    }),
    textInputSection: (focus) => ({
        fontSize:20,
        color:'black',
        borderBottomWidth:2,
        borderBottomColor:focus?PRIMARY:'grey',
    })
});

