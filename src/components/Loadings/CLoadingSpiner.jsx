//import liraries
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Spinner } from 'native-base';
import {PRIMARY} from '../../services/styles/color'

// create a component
const CLoadingSpiner = ({size}) => {
    return (
        <View style={styles.container(size)}>
            <Spinner color={PRIMARY} size={size?size*10:30} />
        </View>
    );
};

//make this component available to the app
export default React.memo(CLoadingSpiner);

// define your styles
const styles = StyleSheet.create({
    container: (size) => ({
        backgroundColor:'white', 
        height:size?size*10:35, 
        width:size?size*10:35, 
        elevation:8, 
        alignItems:'center', 
        justifyContent:'center', 
        borderRadius:30, 
        alignSelf:'center', 
        marginVertical:10,
    }),
});

