//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import Overlay from 'react-native-modal-overlay';
import { Spinner } from 'native-base';
import CLoadingSpiner from './CLoadingSpiner';

const { width } = Dimensions.get('screen')
// create a component
const CLoadingOverlay = ({visible, onClose, closeOnTouchOutside, message}) => {
    return (
        <Overlay visible={visible} onClose={onClose} 
            closeOnTouchOutside={closeOnTouchOutside} 
            containerStyle={{ backgroundColor: 'rgba(255,255,255,0.92)' }} 
            childrenWrapperStyle={{backgroundColor:'transparent'}}>
            {/* <CLoadingSpiner /> */}
            <Spinner color='dodgerblue' size={50} />
            {message && (
                <Text style={{ color: 'black', fontWeight: 'bold' }}>{message}</Text>
            )}
        </Overlay>
    );
};

// define your styles
const styles = StyleSheet.create({
  
});

//make this component available to the app
export default React.memo(CLoadingOverlay);
