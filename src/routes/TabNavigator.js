import React from 'react'
import {View} from 'react-native'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import HomeScreen from '../modules/Home/Screens/HomeScreen'
import ProfileScreen from '../modules/Profile/Screens/ProfileScreen'
import Icon from 'react-native-vector-icons/AntDesign';

const screenOptionsConfig = ({ navigation, route }) => ({
    tabBarIcon: ({ focused, color, size }) => {
        let iconName;
        size = focused ? 35:25
        switch (route.name) {
            case 'Beranda':
                iconName = focused ? 'home' : 'home'
                break;
            case 'Profile':
                iconName = focused ? 'user' : 'user';
                break;
            default:
                iconName = focused ? 'user' : 'user'
        }
        return (
            <View>
                <Icon name={iconName} size={size} style={!focused ? { color: 'gray', fontWeight: 'bold' } : { fontSize: size, color: color }} />
            </View>
        )
    }
})

const tabBarOptionsConfig = {
    showLabel: true,
    activeTintColor: 'navy',
    inactiveTintColor: 'dimgray',
    labelStyle: {
        fontSize: 12,
        fontWeight: 'bold'
    }
}

const Tab = createBottomTabNavigator()

const TabNavigator = () => {
    return(
        <Tab.Navigator tabBarOptions={tabBarOptionsConfig} screenOptions={screenOptionsConfig}>
            <Tab.Screen name='Beranda' component={HomeScreen} />
            <Tab.Screen name='Profil' component={ProfileScreen} />
        </Tab.Navigator>
    )
}

export default TabNavigator