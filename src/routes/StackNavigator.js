import React from 'react' 
import { Easing, LogBox } from 'react-native'
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack'
import WelcomeScreen from '../screens/WelcomeScreen'
import SigninScreen from '../modules/Authentication/Screens/SigninScreen'
import TabNavigator from './TabNavigator'
import PresenceScreen from '../modules/Presence/Screens/PresenceScreen'
import { PRIMARY } from '../services/styles/color'

LogBox.ignoreLogs([`Warning: Can't perform a React state update on an unmounted component.`]);
const openConfig = {
    animation: 'spring',
    config: {
        stiffness: 1250,
        damping: 75,
        mass: 3,
        overshootClamping: true,
        restDisplacementThreshold: 0.10,
        restSpeedThreshold: 0.10,
    },
};
const closeConfig = {
    animation: 'timing',
    config: {
        duration:150,
        Easing:Easing.linear
    },
};

const screenOptionsConfig = {
    headerShown: false,
    gestureEnabled: true,
    gestureDirection: 'horizontal',
    cardOverlayEnabled: true,
    ...TransitionPresets.SlideFromRightIOS,
    transitionSpec:{
        open:openConfig,
        close:closeConfig
    }
}

const Stack = createStackNavigator()
const StackNavigator = () => {
    return(
        <Stack.Navigator screenOptions={screenOptionsConfig} animation={true}>
            <Stack.Screen name='Welcome' component={WelcomeScreen} />
            <Stack.Screen name='Signin' component={SigninScreen} />
            <Stack.Screen name='Index' component={TabNavigator} />
            <Stack.Screen name='Presence' component={PresenceScreen} />
        </Stack.Navigator>
    )
}

export default StackNavigator