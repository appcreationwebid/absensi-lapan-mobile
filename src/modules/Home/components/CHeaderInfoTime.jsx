//import liraries
import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import moment from 'moment'
import 'moment/locale/id'

// create a component
const CHeaderInfoTime = () => {
    // console.log('CHeaderInfoTime')
    const [currentTime, setCurrentTime] = useState(null)
    const [currentDay, setCurrentDay] = useState(null)
    const daysArray = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']

    useEffect(() => {
        getCurrentTime()
        let timer = setInterval(() => {
            getCurrentTime()
        },1000)
        return () => {
            clearInterval(timer)
        };
    }, []);

    const getCurrentTime = () => {
        let hour = new Date().getHours();
        let minutes = new Date().getMinutes();
        let seconds = new Date().getSeconds();
        let am_pm = 'PM';

        switch (true) {
            case minutes < 10:
                minutes = '0' + minutes;
           
            case minutes < 10:
                seconds = '0' + seconds;
          
            case hour > 12:
                hour = hour - 12;
           
            case hour == 0:
                hour = 12;
          
            default:
        }

        if (new Date().getHours() < 12) {
          am_pm = 'AM';
        }
        setCurrentTime(hour + ':' + minutes + ':' + seconds + ' ' + am_pm)
    
        daysArray.map((item, key) => {
          if (key == new Date().getDay()) {
            setCurrentDay(item.charAt(0).toUpperCase()+item.slice(1))
          }
        })
      }
    const date= new Date()
    return (
        <View style={styles.container}>
            <View style={styles.titleContainer}>
                <Text style={styles.titleSection}>Live Attendance</Text>
            </View>
            <View style={styles.timeDayContainer}>
                <Text style={styles.timeSection}>{moment(date).format('LTS')}</Text>
                <Text style={styles.daySection}>{moment(date).format('dddd')}, {moment(date).format('LL')}</Text>
            </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        alignItems:'center'
    },
    titleContainer:{
        alignItems:'center',
        marginBottom:10,
        marginTop:10
    },
    titleSection:{
        color:'white',
        fontWeight:'bold',
        fontSize:20
    },
    timeDayContainer:{
        marginTop:25,
        alignItems:'center'
    },
    timeSection:{
        color:'white',
        fontWeight:'bold',
        fontSize:25
    },
    daySection:{
        color:'white',
        fontWeight:'bold',
        fontSize:15
    }
});

//make this component available to the app
export default CHeaderInfoTime;
