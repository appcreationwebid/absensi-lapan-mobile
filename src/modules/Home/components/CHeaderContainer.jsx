//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { PRIMARY } from '../../../services/styles/color';
import CCardAction from './CCardAction';
import CHeaderInfoTime from './CHeaderInfoTime';

// create a component
const CHeaderContainer = ({navigation}) => {
    return (
        <View style={styles.container}>
            <Image style={styles.imageSection} source={require('../../../../assets/images/bg-1.jpg')} />
            <CHeaderInfoTime />
            <CCardAction navigation={navigation} />
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        backgroundColor: PRIMARY,
    },
    imageSection: {
        position:'absolute',
        resizeMode:'cover',
        width: '120%',
        alignSelf: 'center',
        bottom: 100,
        borderBottomLeftRadius: 500,
        borderBottomRightRadius: 500,
        opacity:0.2
    },
});

//make this component available to the app
export default CHeaderContainer;
