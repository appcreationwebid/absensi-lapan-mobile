//import liraries
import { Button } from 'native-base';
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { useSelector } from 'react-redux';
import CButtonRegular from '../../../components/Buttons/CButtonRegular';

const {width} = Dimensions.get('screen')
// create a component
const CCardAction = ({navigation}) => {
    const profileState = useSelector(state => state.profile) 
    const profile = profileState?.profile
    const divisi = profile?.divisi
    const jam_kerja = profile?.jam_kerja
    const paramNavigationClockIn = {
        type:'clockin',
        name:'Clock In'
    }
    const paramNavigationClockOut = {
        type:'clockout',
        name:'Clock Out'
    }
    return (
        <View style={styles.container}>
            <View style={styles.profileInfoContainer}>
                <Text style={styles.nameSection}>Hi, {profile?.nama_lengkap}</Text>
                <Text style={styles.divisiSection}>{divisi?.nama}</Text>
            </View>
            <View style={styles.infoContainer}>
                <Text style={styles.infoSection}>-{jam_kerja?.nama}-</Text>
                <Text style={styles.infoSection}>Jam Masuk : {jam_kerja?.jam_masuk_mulai} - {jam_kerja?.jam_masuk_selesai}</Text>
                <Text style={styles.infoSection}>Jam Pulang : {jam_kerja?.jam_pulang_mulai} - {jam_kerja?.jam_pulang_selesai}</Text>
            </View>
            <View style={styles.line}></View>
            <Text></Text>
            <View style={styles.buttonContainer}>
                <CButtonRegular width={8} title='Clock In' onPress={() => navigation.navigate('Presence', paramNavigationClockIn)} />
                <CButtonRegular width={8} title='Clock Out' onPress={() => navigation.navigate('Presence', paramNavigationClockOut)} />
            </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        backgroundColor:'white',
        padding:15,
        margin:15,
        borderRadius:15
    },
    profileInfoContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        margin:5
    },
    nameSection:{
        color:'black',
        fontSize:17,
        fontWeight:'bold'
    },
    divisiSection:{
        color:'dimgray',
        fontSize:17,
        fontWeight:'bold'
    },
    infoContainer:{
        alignItems:'center',
        margin:10
    },
    infoSection:{
        color:'dimgrey',
        fontSize:16,
        fontWeight:'bold'
    },
    line:{
        alignSelf:'center',
        backgroundColor:'darkgrey',
        width:width/1.1,
        height:1
    },
    buttonContainer:{
        margin:5,
        flexDirection:'row',
        justifyContent:'space-between'
    }
});

//make this component available to the app
export default CCardAction;
