//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import moment from 'moment'
import 'moment/locale/id'

const {width} = Dimensions.get('screen')
// create a component
const CAttendanceItem = ({time, type}) => {
    const date= new Date('2021-05-10T11:31:54+00:00')
    return (
        <View style={styles.container}>
            <View style={styles.dataContainer}>
                <Text style={[styles.itemSection,{color:'black', fontWeight:'bold', width:200}]} numberOfLines={1}>{time}</Text>
                <Text style={[styles.itemSection, {color:'dimgray'}]}>{type}</Text>
                <Icon name='right' size={20} color='dimgray' />
            </View>
            <View style={styles.divider}></View>
        </View>
    );
};
CAttendanceItem.defaultProps = {
    time: moment('2021-05-10T11:31:54+00:00').format('LLL'),
    type: 'Clock Type'
}

// define your styles
const styles = StyleSheet.create({
    container: {
        marginVertical:5
    },
    dataContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    itemSection:{
        fontSize:15
    },
    divider:{
        marginVertical:10,
        alignSelf:'center',
        backgroundColor:'darkgrey',
        width:width,
        height:1
    },
});

//make this component available to the app
export default React.memo(CAttendanceItem);
