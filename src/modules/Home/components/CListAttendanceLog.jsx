//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import CAttendanceItem from './CAttendanceItem';

// create a component
const CListAttendanceLog = () => {
    return (
        <View style={styles.container}>
            <View style={styles.titleContainer}>
                <Text style={styles.titleSection}>Attendance Log</Text>
                <Text style={styles.subTitleSection}>Log History</Text>
            </View>
            <View style={styles.listAttendanceContainer}>
                {Array(10).fill('').map((_,i) => (
                    <CAttendanceItem key={i} />
                ))}
            </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        marginHorizontal:20,
        marginTop:20
    },
    titleContainer:{
        flexDirection:'row',
        justifyContent:'space-between'
    },
    titleSection:{
        fontWeight:'bold',
        fontSize:20
    },
    subTitleSection:{
        color:'grey',
        fontSize:17,
        fontWeight:'bold'
    },
    listAttendanceContainer:{
        marginVertical:20,
    }
});

//make this component available to the app
export default CListAttendanceLog;
