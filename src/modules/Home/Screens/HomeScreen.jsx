import React, { Component } from 'react'
import { Text, StyleSheet, View, StatusBar, ScrollView, FlatList } from 'react-native'
import { connect } from 'react-redux'
import CLoadingSpiner from '../../../components/Loadings/CLoadingSpiner'
import API from '../../../services/api/API'
import { PRIMARY } from '../../../services/styles/color'
import CAttendanceItem from '../components/CAttendanceItem'
import CHeaderContainer from '../components/CHeaderContainer'
import CListAttendanceLog from '../components/CListAttendanceLog'

class HomeScreen extends Component {
    constructor() {
        super()
        this.state = {
            data: [],
            page: 1,
            isLoading: false,
            endLoad: false,
            refresh: false
        }
    }

    componentDidMount() {
        this.setState({ isLoading: true }, this.getData)
    }

    getData = async () => {
        const apiURL = `https://jsonplaceholder.typicode.com/posts?_limit=10&_page=${this.state.page}`
        const response = await API.get(apiURL)
            if (response?.data) {
                console.log(response.data.length)
                this.setState({
                    data: [...this.state.data, ...response.data],
                    isLoading: false,
                    refresh:false
                })
                if (response.data.length == 0) {
                    console.log('tidak ada lagi data')
                    this.setState({ endLoad: true })
                }
            }
    }

    renderItem = ({ item }) => {
        return (
            <View style={styles.listAttendanceContainer}>
                <CAttendanceItem time={item.title} type={item.id} />
            </View>
        )
    }
    renderHeader = () => (
        <View style={styles.titleContainer}>
            <Text style={styles.titleSection}>Attendance Log</Text>
            <Text style={styles.subTitleSection}>Log History</Text>
        </View>
    )
    renderFooter = () => {
        return this.state.isLoading ? <CLoadingSpiner size={3} style={{margin:500}} /> : <Text style={{ textAlign: 'center', marginBottom: 25 }}>Tidak tersedia lagi data</Text>
    }

    handleLoadMore = async () => {
        if (!this.state.endLoad) {
            console.log('render')
            this.setState({
                page: this.state.page + 1,
                isLoading: true
            }, this.getData)
        } else {
            console.log('stop render')
        }
    }

    handleRefresh = async () => {
        this.setState({refresh:true})
        setTimeout(() => {    this.setState({
                data: [],
                page: 1,
                endLoad:false
            }, this.getData)
        },1500)
    }

    render() {
        console.log('HomeScreen')
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={PRIMARY} />
                <CHeaderContainer {...this.props} />

                <FlatList data={this.state.data} keyExtractor={(item => item.id.toString())}
                    onEndReached={this.handleLoadMore} onEndReachedThreshold={0.5} initialNumToRender={100}
                    ListHeaderComponent={this.renderHeader}
                    ListFooterComponent={this.renderFooter}
                    renderItem={(this.renderItem)}
                    showsVerticalScrollIndicator={false}
                    onRefresh={this.handleRefresh}
                    refreshing={this.state.refresh}
                />


                {/* <Text>{JSON.stringify(this.state.data)}</Text> */}
                {/* <ScrollView>
                    <CListAttendanceLog />
                </ScrollView> */}
            </View>
        )
    }
}

const mapStateToProps = (state) => ({
    profileState: state.profile,
    listPresenceState: state.listPresence
})


export default connect(mapStateToProps)(HomeScreen)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    titleContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 20,
        marginHorizontal: 15
    },
    titleSection: {
        fontWeight: 'bold',
        fontSize: 20
    },
    subTitleSection: {
        color: 'grey',
        fontSize: 17,
        fontWeight: 'bold'
    },
    listAttendanceContainer: {
        marginHorizontal: 15
    }
})
