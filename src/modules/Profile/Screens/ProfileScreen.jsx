import React, { Component } from 'react'
import {StyleSheet, View } from 'react-native'
import { Button, Text } from 'native-base'
import { connect } from 'react-redux'
import { SET_LOGOUT } from '../../../services/constant/constantReducer'

class ProfileScreen extends Component {
    handleLogout = () => {
        this.props.navigation.replace('Signin')
        setTimeout(() => {
            this.props.logoutAction()
        },1000)
    }
    render() {
        return (
            <View style={styles.container}>
                <Button block onPress={this.handleLogout} style={{backgroundColor:'#d90429'}}>
                    <Text>Logout</Text>
                </Button>
            </View>
        )
    }
}

const mapStateToProps = (state) => ({
    loadingState: state.loading
})

const mapDispatchToProps = (dispatch) => ({
    logoutAction: () => dispatch({type:SET_LOGOUT})
})

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen)

const styles = StyleSheet.create({
    container:{

    }
})
