import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, TouchableOpacity, ScrollView, StatusBar } from 'react-native'
import { connect } from 'react-redux'
import CButtonPill from '../../../components/Buttons/CButtonPill'
import CInputFloatLabel from '../../../components/Inputs/CInputFloatLabel'
import { PRIMARY } from '../../../services/styles/color'
import { SubmitSignin } from '../actions/ActAuth'
import CCardForm from '../Components/CCardForm'

class SigninScreen extends Component {
    state = {
        username:'',
        password:''
    }
    handleChangeText = inputType => value => {
        this.setState({
            [inputType]:value
        })
    }

    handleSubmit = () => {
        const {username, password} = this.state
        this.props.SubmitSignin(username, password, this.props.navigation)
    }

    render() {
        const {username, password} = this.state
        const {loadingState:{signinLoading}} = this.props
        return (
            <ScrollView style={styles.container}>
                <StatusBar backgroundColor={PRIMARY} /> 
                <View style={styles.bgContainer}>
                    <Image style={styles.imageSection} source={require('../../../../assets/images/bg-2.png')} />
                </View>
                <View style={styles.formContainer}>
                    <CCardForm>
                        <View style={styles.formSection}>
                            <CInputFloatLabel label='Email/Username' 
                                onChangeText={this.handleChangeText('username')} keyboardType='email-address'
                                value={username} textContentType='emailAddress' autoCapitalize='none' />
                            <CInputFloatLabel label='Kata Sandi' password 
                                style={{ marginTop:25 }} onChangeText={this.handleChangeText('password')} 
                                value={password} autoCapitalize='none' />
                        </View>
                        <TouchableOpacity style={styles.forgotPassContainer}>
                            <Text style={styles.forgotPassText}>Lupa Kata Sandi</Text>
                        </TouchableOpacity>
                        <View style={styles.buttonSection}>
                            <CButtonPill onPress={this.handleSubmit} loading={signinLoading} />
                        </View>
                    </CCardForm>
                    <View style={styles.footerContainer}>
                        <Text style={{ fontWeight:'bold' }}>Fingerprint Base Online Attendance</Text>
                        <Text>absenlapan.appcreation.web.id</Text>
                        <Text>Vesi 1.0.0 (Beta)</Text>
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const mapStateToProps = (state) => ({
    loadingState: state.loading
})

const mapDispatchToProps = (dispatch) => ({
    SubmitSignin: (username, password, navigation) => dispatch(SubmitSignin(username,password,navigation))
})

export default connect(mapStateToProps, mapDispatchToProps)(SigninScreen)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    bgContainer: {
        backgroundColor: 'ghostwhite',
        height: 300,
        width: '100%',
        borderBottomLeftRadius: 100,
        borderBottomRightRadius: 100,
    },
    imageSection: {
        resizeMode:'contain',
        width: '150%',
        alignSelf: 'center',
        bottom: 225,
        borderBottomLeftRadius: 500,
        borderBottomRightRadius: 500,
    },
    formContainer: {
        bottom: 35
    },
    formSection:{
        margin:15
    },
    buttonSection:{
        margin:15,
        marginBottom:50
    },
    forgotPassContainer:{
        marginHorizontal:30,
        marginVertical:10,
        alignSelf:'flex-end'
    },
    forgotPassText:{
        fontSize:17,
        fontWeight: 'bold',
        color:PRIMARY
    },
    footerContainer:{
        alignSelf:'center',
        alignItems:'center'
    }
})
