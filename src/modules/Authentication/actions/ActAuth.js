import { Alert, Keyboard } from "react-native"
import CToastAndroid from "../../../components/Toasts/CToastAndroid"
import API,{SIGNIN} from "../../../services/api/API"
import { SET_LOGIN, SIGNIN_LOADING, SIGNIN_UNLOADING } from "../../../services/constant/constantReducer"
import { Store } from "../../../services/redux/Store"


const SubmitSignin = (username, password, navigation) => async dispatch => {
    Keyboard.dismiss()
    if(username!==''||password!==''){
        try {
            dispatch({type:SIGNIN_LOADING})
            const data = {email: username, password: password}
            const response = await API.post(SIGNIN, data)
            if(response){
                console.log(response.data)
                dispatch({type:SIGNIN_UNLOADING})
                if(response.data.success){
                    const loginData = {...response.data.output_data, loggedIn:true}
                    Store.dispatch({type:SET_LOGIN, payload:loginData})
                    navigation.replace('Index')
                }else{
                    CToastAndroid('Email / Kata sandi salah', 'short')
                }
            }
        } catch (error) {
            console.log(error.response.data.message)
            dispatch({type:SIGNIN_UNLOADING})
            Alert.alert('Masalah Server', 'Mohon maaf terjadi kesalahan pada server, coba lagi nanti!')
        }
    }else{
        CToastAndroid('Pastikan email & password tidak kosong', 'short')
    }
}

export {SubmitSignin}