//import liraries
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { PRIMARY } from '../../../services/styles/color';

// create a component
const CCardForm = ({children}) => {
    console.log('CCardForm')
    return (
        <View style={styles.container}>
            <View style={styles.titleContainer}>
                <Text style={styles.titleSection}>LAPAN ATTENDANCE ONLINE</Text>
                <Text style={styles.titleSection}>LOGIN AKUN</Text>
            </View>
            {children}
        </View>
    );
};

//make this component available to the app
export default React.memo(CCardForm);

// define your styles
const styles = StyleSheet.create({
    container: {
        margin:15,
        backgroundColor: 'white',
        elevation:10,
        borderRadius:15
    },
    titleContainer:{
        margin:10,
        alignSelf:'center',
        alignItems:'center'
    },
    titleSection:{
        fontSize:20,
        fontWeight:'bold',
        color:PRIMARY
    },
});

