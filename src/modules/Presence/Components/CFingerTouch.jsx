//import liraries
import React, { Fragment } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';

// create a component
const CFingerTouch = ({onPress, support}) => {
    return (
        <View style={styles.container}>
            {support && (
                <TouchableOpacity style={styles.container} onPress={onPress}>
                    <Image source={require('../../../../assets/images/fingerprint.png')} style={{resizeMode:'stretch', width:250, height:300}} />
                    <Text style={styles.textSection}>Sentuh Untuk Mulai Mengabsen</Text>
                </TouchableOpacity>
            )}
            {!support && (
                <Fragment>
                    <Image source={require('../../../../assets/images/denied.png')} style={{width:300, height:300}} />
                    <Text style={styles.textSection}>Perangkat Tidak Mendukung</Text>
                </Fragment>
            )}
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    },
    textSection:{
        fontWeight: 'bold',
        color:'dimgrey',
        marginTop:10
    }
});

//make this component available to the app
export default CFingerTouch;
