//import liraries
import React, { Component } from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import { Card, CardItem, Body, Text } from 'native-base';

const {width} = Dimensions.get('screen')
// create a component
const CInfoPresence = () => {
    return (
        <View style={styles.container}>
            <Card>
            <CardItem>
              <Body>
                <View style={styles.textItemContainer}>
                    <Text>✔ </Text>
                    <Text>Pastikan perangkat anda mendukung biometrik</Text>
                </View>
                <View style={styles.textItemContainer}>
                    <Text>✔ </Text>
                    <Text>Pastikan sidik jari anda sudah di konfigurasi di pengaturan perangkat </Text>
                </View>
                <View style={styles.textItemContainer}>
                    <Text>✔ </Text>
                    <Text>Pastikan ID Biometrik perangkat berhasil di daftarkan ke server</Text>
                </View>
                <View style={styles.textItemContainer}>
                    <Text>✔ </Text>
                    <Text>Pastikan login dengan akun anda sendiri di saat pendaftaran ID Biometrik perangkat</Text>
                </View>
              </Body>
            </CardItem>
          </Card>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
 
    },
    textItemContainer:{

        flexDirection: 'row',
        marginVertical:5,
        width: width-50
        
    }
});

//make this component available to the app
export default CInfoPresence;
