//import liraries
import React, { useState, useEffect, Fragment } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import {Container} from 'native-base'
import PresenceContent from './PresenceContent';
import CLoadingSpiner from '../../../components/Loadings/CLoadingSpiner'
// create a component
const PresenceScreen = ({route, navigation, props}) => {
    const [content, setContent] = useState(false)
    const {name} = route.params
    useEffect(() => {
        setTimeout(() => {
            setContent(true)
        },500)
    }, []);
    return (
        <Container>
            {content?<PresenceContent navigation={navigation} paramName={name} /> :<CLoadingSpiner />}
            {/* <PresenceContent navigation={navigation} paramName={name} /> */}
        </Container>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        
    },
});

//make this component available to the app
export default PresenceScreen;
