import React, { Component, Fragment } from 'react'
import { StyleSheet, View, Alert } from 'react-native'
import { Text, Header, Left, Body, Right, Title, Button, Icon, Content } from 'native-base'
import ReactNativeBiometrics from 'react-native-biometrics'
import { connect } from 'react-redux'
import CLoadingOverlay from '../../../components/Loadings/CLoadingOverlay'
import { CLEAR_BIOMETRIC_ID, PRESENCE_LOADING, PRESENCE_UNLOADING, SET_BIOMETRIC_ID, SET_LOGOUT } from '../../../services/constant/constantReducer'
import { PRIMARY } from '../../../services/styles/color'
import CInfoPresence from '../Components/CInfoPresence'
import CFingerTouch from '../Components/CFingerTouch'
import API, { HIT_PRESENCE, REGISTER_BIOMETRIC_ID } from '../../../services/api/API'
import { NavigationActions, StackActions } from '@react-navigation/native';

class PresenceContent extends Component {
    constructor() {
        super()
    }
    state = {
        support: true
    }
    async componentDidMount() {
        ReactNativeBiometrics.isSensorAvailable()
            .then((resultObject) => {
                const { available, biometryType } = resultObject
                switch (true) {
                    case available && biometryType === ReactNativeBiometrics.TouchID:
                        console.log('TouchID is supported')
                        break;
                    case available && biometryType === ReactNativeBiometrics.FaceID:
                        console.log('FaceID is supported')
                        break;
                    case available && biometryType === ReactNativeBiometrics.Biometrics:
                        this.props.setLoading('Mendaftarkan ID Biometrik Perangkat Ke Server')
                        this.keyNotExists().then(result => {
                            this.handleCreateKeys()
                        }).catch(err => {
                            console.log(err)
                            this.props.setUnloading()
                        })
                        console.log('Biometrics is supported')
                        break;
                    default:
                        Alert.alert('Mohon Maaf', 'Fitur biometrics tidak support di perangkat anda')
                        console.log('Biometrics not supported')
                        this.setState({ support: false })
                        break;
                }
            })
    }

    keyNotExists = () => {
        return new Promise((resolve, reject) => {
            ReactNativeBiometrics.biometricKeysExist()
                .then((resultObject) => {
                    const { keysExist } = resultObject
                    if (keysExist) {
                        reject('Keys exists')
                    } else {
                        resolve('Keys Not Exists')
                    }
                })
        })
    }

    handleCreateKeys = () => {
        ReactNativeBiometrics.createKeys()
            .then((resultObject) => {
                const { publicKey } = resultObject
                this.sendBiometricToServer(publicKey)
            }).catch(error => {
                Alert.alert('Konfigurasi Gagal', 'Terjadi masalah saat mengambil ID Biomentrik perangkat')
                console.log('Create Keys : ' + error)
            })
    }

    handleDeleteKey = () => {
        ReactNativeBiometrics.deleteKeys()
            .then((resultObject) => {
                const { keysDeleted } = resultObject
                if (keysDeleted) {
                    this.props.clearBiometricID()
                    console.log('Successful deletion')
                } else {
                    console.log('Unsuccessful deletion because there were no keys to delete')
                }
            })
    }
    handleCreateSignature = () => {
        ReactNativeBiometrics.createSignature({
            promptMessage: `${this.props.paramName}\nTempelkan jari ke sensor`,
            payload: 'payload data'
        }).then((resultObject) => {
            const { success, signature } = resultObject
            if (success) {
                this.props.setLoading('Validasi Sidik Jari')
                console.log(signature)
                this.submitPresence()
                // verifySignatureWithServer(signature, payload)
            }
        })
    }

    //START::INTEGRATE SERVER
    sendBiometricToServer = async (biometric_key) => {
        const data = { biometric_id: biometric_key }
        const config = {
            headers: { Authorization: `Bearer ${this.props.profileState.token}` }
        };
        try {
            const response = await API.post(REGISTER_BIOMETRIC_ID, data, config)
            if (response?.data?.success) {
                this.props.setBiometricID(biometric_key)
                Alert.alert('Konfigurasi Berhasil', response.data.message)
                this.props.setUnloading()
            } else {
                Alert.alert('Mohon Maaf', response.data.message)
                this.props.setUnloading()
                this.handleDeleteKey()
                this.props.navigation.goBack()
            }
            console.log(response.data)
        } catch (error) {
            console.log(error.response.data)
            Alert.alert('Mohon Maaf', 'Terjadi masalah pada server')
            this.props.setUnloading()
            this.handleDeleteKey()
        }

    }

    submitPresence = async () => {
        // setTimeout(() => {
        //     this.props.setUnloading()
        //     Alert.alert('Absen Diterima', 'Terimakasih, absen anda berhasil diterima.')
        //     this.props.navigation.goBack()
        // }, 2000)
        console.log('Biometrik ID :' + this.props.profileState.biometric_id)
        const config = {
            headers: { Authorization: `Bearer ${this.props.profileState.token}` }
        };
        const data = { biometric_id: this.props.profileState.biometric_id }
        try {
            const response = await API.post(HIT_PRESENCE, data, config)
            if (response?.data?.success) {
                Alert.alert('Absen Diterima', response.data.message);
                this.props.setUnloading();
                this.props.navigation.goBack()
            } else {
                this.props.setUnloading();
                if (response.data.output_data) {
                    Alert.alert('Absen Gagal', response.data.message);
                    this.handleDeleteKey()
                    this.props.setLogout()
                    // this.props.navigation.navigate('Welcome')
                    // this.props.navigation.dispatch(StackActions.pop(1))
                    this.props.navigation.goBack()
                    this.props.navigation.replace('Signin')
                }
                if (!response.data.output_data) {
                    Alert.alert('Absen Gagal', response.data.message);
                    this.props.setUnloading();
                    this.props.navigation.goBack()
                }
            }
            console.log(response.data);
        } catch (error) {
            console.log(error);
            Alert.alert('Mohon Maaf', 'Terjadi masalah pada server');
            this.props.setUnloading()
        }
    }

    handleSubmitDeleteKey = () => {
        // this.handleDeleteKey()
        Alert.alert('Hubungi Administrator!!!',
            'Pengajuan hapus kunci perangkat merupakan penghapusan data kunci biometrik di server, tindakan ini dilakukan jika ingin merubah atau mendaftarkan ulang kunci biometrik perangkat ini dengan akun yang lain atau baru'
        )
    }
    //END::INTEGRATE SERVER
    render() {
        console.log('PresenceContent')
        const { navigation, paramName } = this.props
        const { loadingMessage, presenceLoading } = this.props.loadingState
        return (
            <Fragment>
                <Header androidStatusBarColor={PRIMARY} style={{ backgroundColor: PRIMARY }}>
                    <Left>
                        <Button transparent onPress={() => navigation.goBack()}>
                            <Icon name='arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>{paramName}</Title>
                    </Body>
                    <Right />
                </Header>
                {/* <Text> Presence Screen {JSON.stringify(this.props.profileState)}</Text> */}
                <CLoadingOverlay visible={presenceLoading} message={loadingMessage} />
                <Content padder>
                    <CInfoPresence />
                    <View style={styles.fingerTouchSection}>
                        <CFingerTouch support={this.state.support} onPress={this.handleCreateSignature} />
                    </View>
                </Content>
                <Button block onPress={this.handleSubmitDeleteKey} style={{ backgroundColor: '#d90429' }}>
                    <Text>Ajukan Hapus Kunci Perangkat</Text>
                </Button>
            </Fragment>
        )
    }
}

const mapStateToProps = (state) => ({
    loadingState: state.loading,
    profileState: state.profile
})

const mapDispatchToProps = (dispatch) => ({
    setLogout: () => dispatch({ type: SET_LOGOUT }),
    setLoading: (message) => dispatch({ type: PRESENCE_LOADING, payload: { message } }),
    setUnloading: () => dispatch({ type: PRESENCE_UNLOADING }),
    setBiometricID: (biometricID) => dispatch({ type: SET_BIOMETRIC_ID, payload: biometricID }),
    clearBiometricID: () => dispatch({ type: CLEAR_BIOMETRIC_ID })
})

export default connect(mapStateToProps, mapDispatchToProps)(PresenceContent)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
    },
    fingerTouchSection: {
        alignSelf: 'center',
        margin: 25
    }
})
