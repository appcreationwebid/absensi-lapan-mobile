import React, { Component } from 'react'
import { Text, StyleSheet, View, Image } from 'react-native'
import { connect } from 'react-redux'
import CLoadingSpiner from '../components/Loadings/CLoadingSpiner'

class WelcomeScreen extends Component {

    componentDidMount(){
        setTimeout(() => {
            if(this.props.profileState.loggedIn){
                this.props.navigation.replace('Index')
            }else{
                this.props.navigation.replace('Signin')
            }
        },2000)
    }

    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../../assets/images/bg-1.jpg')} style={styles.imageSection} />
                <CLoadingSpiner />
            </View>
        )
    }
}

const mapStateToProps = (state) => ({
    profileState: state.profile
})

export default connect(mapStateToProps)(WelcomeScreen)

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white',
        alignItems:'center',
        justifyContent:'center'
    },
    imageSection:{
        resizeMode:'contain',
        width:'100%',
        left:20
    }
})
