import { PRESENCE_LOADING, PRESENCE_UNLOADING, SIGNIN_LOADING, SIGNIN_UNLOADING } from "../../constant/constantReducer";

const initialState = {
    loadingMessage: '',
    signinLoading: false,
    presenceLoading: false
}

const LoadingReducer = (state=initialState, action) => {
    switch (action.type) {
        case SIGNIN_LOADING:
            return{
                signinLoading:true,
                loadingMessage: action.payload?action.payload.message:''
            }
        case SIGNIN_UNLOADING:
            return{
                signinLoading:false,
                loadingMessage:''
            }
        case PRESENCE_LOADING:
            return{
                presenceLoading:true,
                loadingMessage: action.payload?action.payload.message:''
            }
        case PRESENCE_UNLOADING:
            return{
                presenceLoading:false,
                loadingMessage:''
            }
        default:
            return state
    }
}

export default LoadingReducer