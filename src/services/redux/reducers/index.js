import {combineReducers} from 'redux'
import ListPresenceReducer from './ListPresenceReducer'
import LoadingReducer from './LoadingReducer'
import ProfileReducer from './ProfileReducer'

const reducer = combineReducers({
    loading: LoadingReducer,
    profile: ProfileReducer,
    listPresence: ListPresenceReducer
})

export default reducer