import { CLEAR_BIOMETRIC_ID, SET_BIOMETRIC_ID, SET_LOGIN, SET_LOGOUT } from "../../constant/constantReducer";

const initialState = {
    biometric_id: '',
    loggedIn: false
}

const ProfileReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_LOGIN:
            return {...state,...action.payload}
        case SET_LOGOUT:
            return {...state, loggedIn:false}
        case SET_BIOMETRIC_ID:
            return { ...state, biometric_id: action.payload }
        case CLEAR_BIOMETRIC_ID: 
            console.log('biometric_is_clear')
            return {...state, biometric_id: ''}
        default:
            return state
    }
}

export default ProfileReducer