import { applyMiddleware, createStore, compose } from "redux";
import thunk from 'redux-thunk'
import {persistStore, persistReducer} from 'redux-persist'
import AsyncStorage from "@react-native-async-storage/async-storage";
import reducer from "./reducers";

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    blacklist:['loading'],
    whitelist:['profile']
}

const persistedReducer = persistReducer(persistConfig, reducer)

let composeEnhanchers = compose

if(__DEV__){
    composeEnhanchers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
}

const Store = createStore(persistedReducer, composeEnhanchers(applyMiddleware(thunk)))
const Persistor = persistStore(Store)

export {Store, Persistor}