import axios from 'axios';

//Base URL API
const BASE_URL = 'https://absenlapan.appcreation.web.id/api/'

//Endpoint
export const SIGNIN = 'login';
export const REGISTER_BIOMETRIC_ID = 'register-biometric';
export const HIT_PRESENCE = 'hit-presence';

export default API = axios.create({
    baseURL: BASE_URL,
    timeout:1000,
    headers: {
        Accept: 'application/json',
    }
});